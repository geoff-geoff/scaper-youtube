#!/usr/bin/env python3

##################
#  VERSION 0.2  #
##################


import sys
import requests
from bs4 import BeautifulSoup
import re
import csv

lien = sys.argv[1]
liens = []  # on recup les liens youtube de l'ordre yt/watch=.......
titres = []
liens_formated = []
commentaires = []
cat = []

# user agent pour recup la page sur youtube
mozhdr = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB;'
          ' rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3'}


def recup_la_page(lien):
    r = requests.get(lien, headers=mozhdr)
    soup = BeautifulSoup(r.text, "html.parser")
    return soup


def find_user(lien):
    r = requests.get(lien, headers=mozhdr)
    r = r.text
    m = re.findall('/user/[\S]+/videos', r)
    return "https://www.youtube.com" + m[0]


def scrapping_all_video(lien):
    address = "https://www.youtube.com"
    liens = []
    soup = recup_la_page(lien)
    yt_links = soup.find_all("a")
    for el in yt_links:
        yt_href = el.get("href")
        if "/watch?v=" in yt_href:
            liens.append(address + yt_href)
    return liens


def le_titre(soup):
    yt_links = soup.find_all("span", class_="watch-title")
    for i in yt_links:
        yt_title = i.get("title")
    return yt_title


def commentaire_video(soup):
    comm = soup.find_all("p", id="eow-description")
    comm[0] = str(comm[0])
    comm[0] = comm[0].replace("<br/>", "<br>")
    # on remplace les links... par leur vrai valeur
    links = re.findall("<a [a-zA-Z0-9=\"\- _:/.?>]+...</a>", str(comm[0]))
    for i in links:
        replaces = re.findall("https?[a-zA-Z0-9:/.?=_]+", i)
        comm[0] = comm[0].replace(replaces[-1], replaces[0])

    comm = (re.sub('<[^b][a-zA-Z0-9 \-=\":\/.?_%&;]+>', " ", str(comm[0]))).strip()
    if "http" in comm:
        liens = re.findall('https?://[\S]+', comm)
        for i in liens:
            comm = comm.replace(i, "<a href="+i+'">'+i+"</a>")
    return comm


"""
def categorie(soup):
    categorie = soup.find_all("a" , class_ = " yt-uix-sessionlink spf-link ")
    link = re.findall('<[a-zA-Z0-9 =\"-]+href=\"',str(categorie[0]))
    print(link)
    link = str(categorie[0]).replace(link[0],'<a href="http://www.youtube.com/')
    return link
"""


def transforme(lien):
    """
    le lien https://www.youtube.com/watch?v=EOn-cuO96Zg
    doit ressembler à ça:
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/EOn-cuO96Zg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
"""
    lien = lien.replace("youtube", "youtube-nocookie")
    lien = lien.replace("watch?v=", "embed/")
    return '<iframe width="560" height="315" src="' + lien + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'

###############################################################################
###############################################################################
# un lien user, donc on va passer sur le lien video puis
# scraper tous ce que l'on peut


if "youtube.com/channel/" in lien:
    lien = find_user(lien)


if "youtube.com/user/" in lien:
    if len(lien.split("/")) == 5:
        lien += "/video"
    else:
        lien = "https://www.youtube.com/user/"+lien.split("/")[4]+"/video"
    liens = scrapping_all_video(lien)


# pour juste une vidéo
if "/watch?v=" in lien:
    liens.append(lien.split("&")[0])


for i in liens:
    soup = recup_la_page(i)
    titre = le_titre(soup)
    titres.append(titre)
    comm = commentaire_video(soup)
    commentaires.append(comm)
    liens_formated.append(transforme(i))
#    cat.append(categorie(soup))

to_csv = (list(zip(liens_formated, titres, commentaires)))
with open('test.csv', 'w') as out:
    csv_out = csv.writer(out)
    csv_out.writerow(['iframe', 'name', 'comm'])
    for row in to_csv:
        csv_out.writerow(row)
